P       = free
OBJECTS =
CFLAGS  = -Wall -O3 -g
LDLIBS  =
CC      = clang

ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

.PHONY: clean install distclean help

$(P): $(OBJECTS)

install:
	install -m 755 $(P) $(PREFIX)/bin

clean:
	$(RM) $(P)

distclean: clean
	rm -rf *.dSYM *.o

uninstall:
	$(RM) $(PREFIX)/bin/$(P)

help:
	@echo 'Usage: make [TARGET]'
	@echo 'TARGETS:'
	@echo '  install   install program.'
	@echo '  uninstall uninstall program.'
	@echo '  clean     clean objects and the executable file.'
	@echo '  distclean clean objects, the executable and dependencies.'
	@echo '  help      print this message.'
