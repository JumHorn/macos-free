# macOS free
A free-like command for macOS. Inspired by the `free` command distributed with GNU/Linux.

## Installation
Installs to /usr/local/bin by default:
```
make && sudo make install
```

Can be installed to an alternate location (ex. ~/.local/bin) by specifying a PREFIX:
```
make && make install PREFIX=~/.local
```

## Examples
### Print memory usage in mebibytes
```
$ free -m
      total   used    free    wire    inac/ac spec    comp    purge
Mem:  16384   7028    647     1643    10642   155     2641    440
Swap: 1024    28      996
```
### Getting help
```
$ free --help
Usage:
 free [option]

Options:
 -b, --bytes	 show output in bytes
     --kilo	 show output in kilobytes
     --mega	 show output in megabytes
     --giga	 show output in gigabytes
     --tera	 show output in terabytes
     --peta	 show output in petabytes
 -k, --kibi	 show output in kibibytes
 -m, --mebi	 show output in mebibytes
 -g, --gibi	 show output in gibibytes
 -t, --tebi	 show output in tebibytes
 -p, --peti	 show output in pebibytes
     --si	 use powers of 1000 not 1024
 -h  --human	 show human-readable output

 -H, --help	 display help and exit
 -V, --version	 display version and exit
 ```
